<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B5SX5rlRXgwxXPe5grXaDKpMlBburp5VdOgfHuW219RN4aFNjescMJy0rVLMHE2Wmd+UnCJOjpIJT82Mwxb1FQ==');
define('SECURE_AUTH_KEY',  '0pOUTjVDy22ArrVPoD8HWTuxF2yBFjCpX6YQLkjE5gEGYE4s1NMPBrn1OYR9h+JKnFTDjQQhqh6m+/9MVFygoQ==');
define('LOGGED_IN_KEY',    'Y5EoTVNoDTYQfsTeDOUeByCXVSNNXQbE0Hwbr9mDBVivDeNFq2rEnFIheUjjsVTu4EEKjb/tfmPTR29XhG9wBA==');
define('NONCE_KEY',        'yuvZBXax2ZZdhKFt201YU1Hz3OXYzE2tYOju7Z5yxcmX399JnSe40/p+qXcE0oDJiwZ7d9kJzCSix+xTULG83w==');
define('AUTH_SALT',        'YXc8JcP4cs1ZWq6u9Uk8R9WT797hcDN19Rn6xiP0Smin1E/dXVPPToi4vOeFyoncEWTcTXv3gl8W6Rx+ppL8NQ==');
define('SECURE_AUTH_SALT', 'JPSUtIvkLplZnDHjS+9GHqLBFroVOTVGf7poPy2mS1szLsAAQo42ha3YjU4PvVcRFOAXrrdpSaeg1maj78pHtw==');
define('LOGGED_IN_SALT',   'aA0G3BfkkpDvJB64xNNMYDHxwdjgGGCNR8CL8xNvqwCvCIGxWIs+sLgzNW0ZqClsKN7nvVSaMFToBKO7Dg4f2w==');
define('NONCE_SALT',       'T7NgYTodifgFFt2qhREVjHNGqmotHYa0aGbm7MIv2yYBntIj2W65VlaRvaZiY2yOYtuC9YpyAXz8v4Mmj6JxmA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
