<?php
/**
 * @package Annuaire Alex
 * @version 1.0.0
 */
/*
Plugin Name: Annuaire Alex
Plugin URI: http://wordpress.org/plugins/annuaire-alex/
Description: Ceci n'est pas juste un plugin, ceci est LE PLUGIN que toute une génération attendait ! Flattez moi, idolatrez moi !
Author: Alexandre Pégourié
Version: 1.7.2
Author URI: http://wordpress.org/plugins/annuaire-alex/
*/

function shortcode_annuaire() {
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT );
    $listeannuaire = "<p> Voici la liste des entreprises</p>";

        foreach($results as $entreprise){
            
            // $a= shortcode_atts (array(
            // 'nom' => $entreprise -> nom_entreprise,
            // 'localisation' =>$entreprise -> localisation_entreprise,
            // ),$atts);
            // return '<p>' .$a['nom'].$a['localisation'];

           $listeannuaire = $listeannuaire ."<div><p>{$entreprise -> id }</p><h2>Entreprise : {$entreprise -> nom_entreprise}</h2>
            <h3>Localisation : {$entreprise -> localisation_entreprise}</h3>
            <h3>Contact : {$entreprise -> prenom_contact} {$entreprise -> nom_contact}, {$entreprise -> mail_contact}</h3></div>";
        }
        return $listeannuaire;
}
        // }
    // }
add_shortcode('annuaire', 'shortcode_annuaire');


